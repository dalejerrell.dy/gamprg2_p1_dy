// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterGameMode.h"
#include "RunCharacterGameMode.h"
#include "Tile.h"
#include "Components/ArrowComponent.h"


void ARunCharacterGameMode::BeginPlay()
{
	Super::BeginPlay();


	for (int i = 0; i < InitialTiles; i++)
	{
		CreateTile();
	}

}

void ARunCharacterGameMode::CreateTile()
{
	//UE_LOG(LogTemp, Warning, TEXT("I just started spawning"));

	FActorSpawnParameters spawnParameters;

	if (TileRef != nullptr)
	{
		//UE_LOG(LogTemp, Warning, TEXT("I have Spawned a new tile"));

		ATile* TileActor = GetWorld()->SpawnActor<ATile>(TileRef, SpawnPoint, FRotator(0), spawnParameters);

		SpawnPoint = FVector(TileActor->GetAttachPoint()->GetComponentLocation());

		TileActor->OnTileExited.AddDynamic(this, &ARunCharacterGameMode::DestroyTile);
	}
}

void ARunCharacterGameMode::DestroyTile(ATile * ToDestroy)
{
	FTimerHandle Timer;

	FTimerDelegate TimerDelegate;

	TimerDelegate.BindLambda( [ToDestroy] {
		ToDestroy->Destroy();
	});

	GetWorldTimerManager().SetTimer(Timer, TimerDelegate, 2.0f, false);

	CreateTile();
}
