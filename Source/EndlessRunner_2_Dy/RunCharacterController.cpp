// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "RunCharacter.h"

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	RunCharacter = Cast<ARunCharacter>(GetCharacter());
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}

void ARunCharacterController::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	MoveForward(1);
}

void ARunCharacterController::MoveRight(float scale)
{

	RunCharacter->AddMovementInput(RunCharacter->GetActorRightVector() * scale);
}

void ARunCharacterController::MoveForward(float scale)
{
	RunCharacter->AddMovementInput(RunCharacter->GetActorForwardVector() * scale);
}
