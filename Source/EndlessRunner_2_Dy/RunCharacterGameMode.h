// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunCharacterGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNER_2_DY_API ARunCharacterGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class ATile> TileRef;

	UPROPERTY(EditAnywhere)
		int32 InitialTiles = 15;

	UPROPERTY(VisibleAnywhere)
		FVector SpawnPoint;

	UFUNCTION()
		void CreateTile();
	
	UFUNCTION()
		void DestroyTile(ATile* ToDestroy);
	
};
