// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTileExitedSigniture, class ATile*, Tile);

UCLASS()
class ENDLESSRUNNER_2_DY_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	UPROPERTY(BlueprintAssignable)
	FTileExitedSigniture OnTileExited;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<class AObstacles>> ObstacleRef;

	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<class APickUps>> PickUpRef;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* Scene;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UArrowComponent* AttachPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		class UBoxComponent* EndTrigger;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnPoints")
		class UBoxComponent* ObstacleSpawnPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnPoints")
		class UBoxComponent* PickUpSpawnPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UChildActorComponent* ChildActor;

	UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* otherActor, UPrimitiveComponent* OtherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void SpawnObstacles();

	UFUNCTION()
		void SpawnPickUp();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UArrowComponent* GetAttachPoint();

	void DestroyActor();

};
