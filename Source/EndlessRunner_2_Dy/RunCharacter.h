// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FActorDeathSignature, class ARunCharacter*, RunCharacter);

UCLASS()
class ENDLESSRUNNER_2_DY_API ARunCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunCharacter();

	UPROPERTY(BlueprintAssignable)
		FActorDeathSignature DeathActor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 TotalCoin;

	UFUNCTION(BlueprintCallable)
		void AddCoin();

	UFUNCTION(BlueprintCallable)
		void Die();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere)
		class USpringArmComponent* SpringArm;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
