// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Obstacles.h"
#include "PickUps.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "Components/ChildActorComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "RunCharacter.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	SetRootComponent(Scene);

	AttachPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("AttachPoint"));
	AttachPoint->SetupAttachment(RootComponent);

	EndTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("EndTrigger"));
	EndTrigger->SetupAttachment(RootComponent);
	EndTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnBeginOverlap);

	ChildActor = CreateDefaultSubobject<UChildActorComponent>("Child");
	ChildActor->SetupAttachment(RootComponent);

	ObstacleSpawnPoint = CreateDefaultSubobject<UBoxComponent>(TEXT("ObstacleSpawnPoint"));
	ObstacleSpawnPoint->SetupAttachment(RootComponent);

	PickUpSpawnPoint = CreateDefaultSubobject<UBoxComponent>(TEXT("PickUpSpawnPoint"));
	PickUpSpawnPoint->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();

	int rand = FMath::RandRange(0, 1);

	if (rand == 0)
		SpawnObstacles();
	else
		SpawnPickUp();
}

void ATile::OnBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * otherActor, UPrimitiveComponent * OtherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (ARunCharacter* Character = Cast<ARunCharacter>(otherActor))
	{
		//UE_LOG(LogTemp, Warning, TEXT("I am trying to spawn another Tile"));
		OnTileExited.Broadcast(this);
	}
}

void ATile::SpawnObstacles()
{
	UE_LOG(LogTemp, Warning, TEXT("I have spawned an Obstacles"));

	FVector randomPointObs;

	randomPointObs = UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawnPoint->GetComponentLocation(), ObstacleSpawnPoint->GetScaledBoxExtent());

	ChildActor->SetChildActorClass(ObstacleRef[FMath::RandRange(0, ObstacleRef.Num() - 1)]);
	
	ChildActor->SetWorldLocation(randomPointObs, false, nullptr, ETeleportType::None);

	ChildActor->CreateChildActor();

}

void ATile::SpawnPickUp()
{
	UE_LOG(LogTemp, Warning, TEXT("I have spawned PickUps"));

	FVector randomPointPickUp;

	randomPointPickUp = UKismetMathLibrary::RandomPointInBoundingBox(PickUpSpawnPoint->GetComponentLocation(), PickUpSpawnPoint->GetScaledBoxExtent());

	ChildActor->SetChildActorClass(PickUpRef[FMath::RandRange(0, PickUpRef.Num() - 1)]);

	ChildActor->SetWorldLocation(randomPointPickUp, false, nullptr, ETeleportType::None);

	ChildActor->CreateChildActor();
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

UArrowComponent * ATile::GetAttachPoint()
{
	return AttachPoint;
}

void ATile::DestroyActor()
{
	if (this != nullptr)
	{
		Destroy();
	}
}

