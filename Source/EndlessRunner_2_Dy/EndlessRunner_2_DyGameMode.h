// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EndlessRunner_2_DyGameMode.generated.h"

UCLASS(minimalapi)
class AEndlessRunner_2_DyGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEndlessRunner_2_DyGameMode();
};



