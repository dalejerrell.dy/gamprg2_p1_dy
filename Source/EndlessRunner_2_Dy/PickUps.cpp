// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUps.h"
#include "RunCharacter.h"

// Sets default values
APickUps::APickUps()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

}

// Called when the game starts or when spawned
void APickUps::BeginPlay()
{
	Super::BeginPlay();
	Mesh->OnComponentBeginOverlap.AddDynamic(this, &APickUps::OnBeginOverlap);
}

void APickUps::OnBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * otherActor, UPrimitiveComponent * OtherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (ARunCharacter* runCharacter = Cast<ARunCharacter>(otherActor))
	{
		OnGet(runCharacter);
		Destroy(this);
	}
}

// Called every frame
void APickUps::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

