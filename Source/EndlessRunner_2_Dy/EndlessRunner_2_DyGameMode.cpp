// Copyright Epic Games, Inc. All Rights Reserved.

#include "EndlessRunner_2_DyGameMode.h"
#include "EndlessRunner_2_DyCharacter.h"
#include "UObject/ConstructorHelpers.h"

AEndlessRunner_2_DyGameMode::AEndlessRunner_2_DyGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
